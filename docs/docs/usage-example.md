```js
local handler = mysql_connect("localhost", "root", "", "db_test")
if (handler)
{
	local result = mysql_query(handler, "SELECT * FROM mdb_users")
 
	if (result)
	{
		print("Query done " + mysql_num_rows(result))
		local row = mysql_fetch_row(result)
		if (row)
		{
			foreach (val in row)
			{
				print(type(val) + " " + val)
			}
		}
 
		mysql_free_result(result)
	}
	else
	{
		print(mysql_error(handler))
		print("Error ID: " + mysql_errno(handler))
	}
 
	result = mysql_query(handler, "SELECT * FROM mdb_users")
 
	if (result)
	{
		print("Query done " + mysql_num_fields(result))
		local row_assoc = mysql_fetch_assoc(result)
		if (row_assoc)
		{
			print(row_assoc["username"] + " " + row_assoc["email"])
		}
 
		mysql_free_result(result)
	}
	else
	{
		print(mysql_error(handler))
		print("Error ID: " + mysql_errno(handler))
	}
 
	mysql_close(handler)
}
```