#pragma once

SQInteger Sq_mysql_connect(HSQUIRRELVM vm);
SQInteger Sq_mysql_close(HSQUIRRELVM vm);
SQInteger Sq_mysql_select_db(HSQUIRRELVM vm);
SQInteger Sq_mysql_query(HSQUIRRELVM vm);
SQInteger Sq_mysql_insert_id(HSQUIRRELVM vm);
SQInteger Sq_mysql_affected_rows(HSQUIRRELVM vm);
SQInteger Sq_mysql_num_rows(HSQUIRRELVM vm);
SQInteger Sq_mysql_num_fields(HSQUIRRELVM vm);
SQInteger Sq_mysql_fetch_row(HSQUIRRELVM vm);
SQInteger Sq_mysql_fetch_assoc(HSQUIRRELVM vm);
SQInteger Sq_mysql_ping(HSQUIRRELVM vm);
SQInteger Sq_mysql_free_result(HSQUIRRELVM vm);
SQInteger Sq_mysql_error(HSQUIRRELVM vm);
SQInteger Sq_mysql_errno(HSQUIRRELVM vm);
SQInteger Sq_mysql_info(HSQUIRRELVM vm);
SQInteger Sq_mysql_stat(HSQUIRRELVM vm);
SQInteger Sq_mysql_sqlstate(HSQUIRRELVM vm);
SQInteger Sq_mysql_warning_count(HSQUIRRELVM vm);
SQInteger Sq_mysql_get_character_set_info(HSQUIRRELVM vm);
SQInteger Sq_mysql_character_set_name(HSQUIRRELVM vm);
SQInteger Sq_mysql_set_character_set(HSQUIRRELVM vm);
SQInteger Sq_mysql_escape_string(HSQUIRRELVM vm);
SQInteger Sq_mysql_real_escape_string(HSQUIRRELVM vm);
SQInteger Sq_mysql_option_reconnect(HSQUIRRELVM vm);