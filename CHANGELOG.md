## Changelog

- Fixed bug in `mysql_fetch_row` that could cause to return improper value if the previous field was `null`.
- Fixed bug in `mysql_fetch_assoc` that could cause to return improper value if the previous field was `null`.
- Big thanks for KimiorV for reporting this bug.